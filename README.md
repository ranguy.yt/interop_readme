# ODCL web app 

A web application developed for manual odcl detection and interop submission. 

## Pipline :
```

             interop  images                                      odlcs
raspberry pi ----------------> ground station ---------> web app --------> interop 
```
##  Setting up auvsi Interop :

official auvsi interop github : https://github.com/suas-competition/interop

for quick setup follow this : https://github.com/ranguy9304/setting-up-interop


### 1. configuring interop port :


```bash
interop
├───client
├───proto
├───server
|    ├───{docker-compose.yml}------open this file 
├───tools
```
 under ports section change the value of port to what you want by default its localhost:8000


## setting up web app :

clone the repo :

```bash
git clone https://gitlab.com/project-manas/ai/auvsi-suas-2023/imaging_pipeline/odcl_web_app.git
```

install the following dependencies 

 ```bash
 sudo apt install nodejs
 sudo apt install npm
 sudo npm -g install create-react-app
 ```

## Building the web app  :

file system of web app:



```bash
odcl_web_app
├───api
|    ├───node_modules
|    ├───public
|    |    ├───params.json ------contains ip and login id for the interop server [change the ip to the ip of interop server]
|    ├───src
|         ├───app.js-----server 
|         ├───utils
|               ├───get_next_image.js
|               ├───submit_interop.js----function to handle api calls for logging into interop and submitting the odlc image
├───client
|    ├───build 
|    ├───node_modules
|    ├───public
|    ├───src
|         ├───assets
|         ├───components
|         |      ├───imgandcrop.jsx-------handles image cropping and sstoring image data 
|         |      ├───selection.jsx--------frontend for submitting odlc (calls endpoints in app.js)
|         ├───scss
|         ├───utils
├───imagingLaunch.sh

```

run npm i inside client and api :

```bash 
cd server 
npm i 
cd ../client 
npm i 
```
build the npm package :
in client directory :

```bash
npm run build
```
npm build everytime you makke changes to the client folder 

Run the backend before frontend :

the server by default runs on localhost:3000

 ```bash 
 node api/src/app.js
 ```
 Run the frontend :

```bash 
serve -s build
```
the web app will run on the specified local host 


In client-build folder make a new folder named "rocket_images" containing images in the following naming format :
"index_lat_long_alt"

example:

1_38.3092322_-76.5569511_155.0.jpg

## Linking interop and webapp :

Open params.json file (refer the directory tree) and change the ip option to the ip where the interop server is running 

## Using the web application :

- manually select the part of the image containing the target, an alert will signify if the image is read to upload or not 

#### select the respective odcl options :

- on the left panel there are two options for the type of object,

- choose the respective options and values for the image.[AN -alpha numeric value ]

- lattitude and longitude of the croped image will be present on the right panel clicking them will copy the values to clipboard.

- after selecting everything press submit , a propt will notify if the image is uploaded successfully or not 

- to check the image data and logs during the upload press f12 in the browser to see client side logs and open the terminal where the server is running to check the server side logs

- status code : 200 signifies everything is done successfully anyother code represents and error which will be logged with the code

## Error handling :

* connection error :

```bash
Error: connect ECONNREFUSED 127.0.0.1:3000
    at TCPConnectWrap.afterConnect [as oncomplete] (node:net:1247:16) {
  errno: -111,
  code: 'ECONNREFUSED',
  syscall: 'connect',
  address: '127.0.0.1',
  port: 3000
}

```

this error is mostly because the web app backend is not able to communicate to the inteop server 
- check if the interop server is up and running 
- check the ip address in params.json file and compare it with the address where the interop server is running 


* status 413 :
 this status error is because the croped image's size exceds the size limit for upload 

